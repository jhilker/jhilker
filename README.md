***
<div align="center">
<h1>
  <sub>
    <img src="https://avatars.githubusercontent.com/u/11656760?v=4" height="44">
  </sub>
  &nbsp;
 jhilker1
  </h1>
  
[![Website](https://img.shields.io/badge/-Website-blue?logo=org&style=for-the-badge&logoColor=white)](https://jhilker.com)
[![Steam](https://img.shields.io/badge/-Steam-black?logo=steam&style=for-the-badge)](https://steamcommunity.com/id/WaitingCynicism/)
[![Gitlab](https://img.shields.io/badge/-jhilker-blue?logo=gitlab&style=for-the-badge&logoColor=white)](https://gitlab.com/jhilker)
[![Email](https://img.shields.io/badge/-Email-blue?logo=gmail&style=for-the-badge&logoColor=white)](mailto:jacob.hilker2@gmail.com)
<!--[![Linkedin](https://img.shields.io/badge/-jhilker-0A66C2?logo=linkedin&style=for-the-badge&logoColor=white)](https://linked.com/in/jhilker)-->
<img src="https://jhilker.com/img/jhilker.jpg" align="right"/>
<!--img src='https://data.whicdn.com/images/354171585/original.gif' width='175px'-->
<br>
<p>
  Hi, I'm Jacob, an avid gamer (both tabletop and video games) and programmer, with my favorite game in particular being Super Metroid from the SNES. I recently graduated from the University of Mary Washington with a major in computer science and a minor in cybersecurity. Outside of programming, I enjoy sound design, worldbuilding, and playing music as a whole (drums, bass, and guitar).
  <br><br>
I'm most comfortable working in Python, though I have experience with Javascript and HTML/CSS.
  </p>
<br>


  </div>
<details>
  <summary><strong>:computer: Main Tech Skills</strong></summary>
  
  [![Python](https://img.shields.io/badge/-python-3776ab?logo=python&style=for-the-badge&logoColor=white)]()
  [![Hugo](https://img.shields.io/badge/-Hugo-ff4088?logo=hugo&style=for-the-badge&logoColor=white)]()
  [![Org-mode](https://img.shields.io/badge/-Orgmode-77aa99?logo=org&style=for-the-badge&logoColor=white)]()
  [![Postgres](https://img.shields.io/badge/-Postgres-4169e1?logo=postgresql&style=for-the-badge&logoColor=white)]()
  [![Java](https://img.shields.io/badge/-Java-007396?logo=java&style=for-the-badge&logoColor=white)]()
  [![Javascript](https://img.shields.io/badge/-Javascript-red?logo=javascript&style=for-the-badge&logoColor=white)]()
  [![Tailwind](https://img.shields.io/badge/-Tailwind-informational?logo=tailwindcss&logoColor=white&style=for-the-badge)]()
  
  </details>
  <details>
  <summary><strong>🎵 Recently Listened To</strong></summary> 
  
  ![Recently Listened To](https://spotify-recently-played-readme.vercel.app/api?user=bqby3nrwzkqhio0yxezv77899&count=3)
  </details>

<details>
  <summary><strong>🎸 Currently Listening To</strong></summary>
  
 [![spotify-github-profile](https://spotify-github-profile.vercel.app/api/view?uid=bqby3nrwzkqhio0yxezv77899&cover_image=true&theme=natemoo-re)](https://spotify-github-profile.vercel.app/api/view?uid=bqby3nrwzkqhio0yxezv77899&redirect=true)
  </details>
